QT += core gui widgets charts

TARGET = Spectral_Mixer
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++14

SOURCES += \
        main.cpp \
        main_window.cpp \
    graphics_view.cpp \
    data.cpp \
    color_mixer.cpp \
    spectrum_chart.cpp

HEADERS += \
        main_window.h \
    graphics_view.h \
    data.h \
    helper.h \
    color_mixer.h \
    spectrum_chart.h

FORMS += \
        main_window.ui

####  include OpenCV  ####

INCLUDEPATH+=/home/tmichels/local_home/opt/opencv/include
INCLUDEPATH+=/home/tmichels/local_home/opt/opencv/include/opencv2

LIBS += -L/home/tmichels/local_home/opt/opencv/lib\
-lopencv_rgbd \
-lopencv_highgui \
-lopencv_imgcodecs \
-lopencv_core \
-lopencv_imgproc
