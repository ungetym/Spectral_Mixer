#ifndef SPECTRUM_CHART_H
#define SPECTRUM_CHART_H
#pragma once

#include <data.h>

#include <QMouseEvent>
#include <QtCharts>

using pairInt = std::pair<int,int>;

class Spectrum_Chart : public QChartView
{
    Q_OBJECT

public:


    Spectrum_Chart(QObject* parent = nullptr);

    void addSeries(QScatterSeries* series, const QColor& draw_color = QColor(0,0,0));

    void addProfile(QLineSeries* profile, const QColor& draw_color = QColor(0,0,0), const bool& is_area = false, const QBrush& brush = QBrush());

    void setChartAxes(float x_min = 400.0, float x_max = 760.0, int x_tick = 7, float y_min = 0.0, float y_max = 1.0, int y_tick = 11);

public slots:


private:
    Data* data_;
    QValueAxis axis_x_, axis_y_;
    QtCharts::QChart chart_;
    std::vector<QScatterSeries*> series_;
    std::vector<QLineSeries*> profiles_;
    std::vector<QAreaSeries*> area_profiles_;

    const int ACTIVE_NONE = -2;
    const int ACTIVE_ALL = -1;

    pairInt active_data_point_idx_ = pairInt(ACTIVE_NONE,ACTIVE_NONE);

    QPointF last_chart_position_;

    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);

    QPointF mouseToChart(const QPoint& position){
        QPointF chart_pos = chart_.mapFromScene(mapToScene(position));
        return chart_.mapToValue(chart_pos);
    }

    QPoint chartToMouse(const QPointF& chart_position){
        return mapFromScene(chart_.mapToPosition(chart_position));
    }

signals:
    void chartChanged();
};

#endif // SPECTRUM_CHART_H
