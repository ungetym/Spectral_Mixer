#include "data.h"
#include "helper.h"

using namespace std;

Image::Image(const QString& name, const int& wavelength, const cv::Mat& image){
    this->setText(name);
    this->setFlags(this->flags() | Qt::ItemIsUserCheckable);
    this->setCheckState(Qt::Checked);
    this->wavelength = wavelength;
    this->image_orig = image.clone();
    cv::cvtColor(image,image_intensity,CV_RGB2GRAY);
    image_intensity.convertTo(image_intensity,CV_32FC1);
    this->setIcon(Helper::cvMatToQPixmap(image));
}

Data::Data(){
    for(int i = 0; i < 3; i++){
        QScatterSeries* scatter_series = new QScatterSeries();
        sensor_coefficients.push_back(scatter_series);
        QLineSeries* line_series = new QLineSeries();
        sensor_profile.push_back(line_series);
    }
}

bool Data::loadInitialProfiles(){

    if(!readProfiles("./emission_profiles.txt", &emission_profiles)){

    }

    if(!readProfiles("./sensor_profiles.txt", &sensor_profiles)){

    }

    return true;
}


bool Data::readProfiles(const string& file_path, vector<Profile>* profiles){
    if(file_path.size()==0 || profiles == nullptr){
        return false;
    }

    ifstream file(file_path);
    if(file.is_open()){
        string line;
        while(getline(file,line)){
            int delim = line.find_first_of(":");
            if(delim != -1){
                Profile profile;
                profile.name = line.substr(0,delim);
                profiles->push_back(profile);
            }
            else{
                profiles->back().data.push_back(vector<QPointF>());
                std::istringstream stream(line.substr(delim+1));
                while (!stream.eof()){
                    float x, y;
                    stream >> x;
                    stream >> y;
                    profiles->back().data.back().push_back(QPointF(x,y));
                }
            }
        }
        file.close();
    }
    else{
        return false;
    }

    return true;
}

bool Data::writeProfiles(const string& file_path, const vector<Profile>& profiles){
    if(file_path.size()==0){
        return false;
    }

    ofstream file(file_path);
    if(file.is_open()){
        for(const Profile& profile : profiles){
            file << profile.name << ":\n";
            for(const vector<QPointF> data : profile.data){
                for(const QPointF& p : data){
                    file << " " << p.x() << " " << p.y();
                }
                file << "\n";
            }
        }
        file.close();
    }
    else{
        return false;
    }

    return true;
}
