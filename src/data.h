#ifndef DATA_H
#define DATA_H
#pragma once

#include <opencv2/opencv.hpp>

#include <QListWidgetItem>
#include <QtCharts>


class Image : public QListWidgetItem
{
public:
    Image(const QString& name, const int& wavelength, const cv::Mat& image);
    cv::Mat image_orig;
    cv::Mat image_intensity;
    int wavelength = 0;
    int check_state = Qt::Checked;
};

struct Profile{
    std::string name;
    int channels;
    std::vector<std::vector<QPointF>> data;
};

class Data : public QObject{

    Q_OBJECT

public:
    Data();

    std::vector<Image> input_images;
    cv::Mat mixed_image;

    QScatterSeries emission_coefficients;
    std::vector<Profile> emission_profiles;
    QLineSeries emission_profile;
    float emission_intensity = 1.0;

    std::vector<QScatterSeries*> sensor_coefficients;
    std::vector<Profile> sensor_profiles;
    std::vector<QLineSeries*> sensor_profile;
    float gain_r = 1.0;
    float gain_g = 1.0;
    float gain_b = 1.0;

    bool loadInitialProfiles();
    bool readProfiles(const std::string& file_path, std::vector<Profile>* profiles);
    bool writeProfiles(const std::string& file_path, const std::vector<Profile>& profiles);

private:

signals:

};

#endif // DATA_H
