#ifndef COLOR_MIXER_H
#define COLOR_MIXER_H
#pragma once

#include "data.h"

#include <QObject>

class Color_Mixer : public QObject
{
    Q_OBJECT
public:
    explicit Color_Mixer(Data* data);

public slots:
    void mix();

private:

    Data* data_;
    std::vector<cv::Mat> mixed_images_ = std::vector<cv::Mat>(3);

signals:

    void mixDone(const cv::Mat& image);

};

#endif // COLOR_MIXER_H
