#ifndef HELPER_H
#define HELPER_H
#pragma once

#include <opencv2/opencv.hpp>

#include <QMessageBox>

namespace Helper{

/// \brief askUser is a simple wrapper for a QMessageBox
/// \param msg          message to display
/// \param button_1     first QMessageBox::StandardButton to show
/// \param button_2     (optional) second button
/// \param button_3     (optional) third button
/// \return             0 if user presses button_1, 1 for button_2 and 2 for button_3
///
inline int askUser(const QString& msg, int button_1, int button_2 = -1, int button_3 = -1){
    QMessageBox dialog;
    dialog.setText(msg);
    if(button_2 != -1){
        if(button_3 != -1){
            dialog.setStandardButtons(static_cast<QMessageBox::StandardButton>(button_1) | static_cast<QMessageBox::StandardButton>(button_2) | static_cast<QMessageBox::StandardButton>(button_3));
        }
        else{
            dialog.setStandardButtons(static_cast<QMessageBox::StandardButton>(button_1) | static_cast<QMessageBox::StandardButton>(button_2));
        }
    }
    else{
        dialog.setStandardButtons(static_cast<QMessageBox::StandardButton>(button_1));
    }
    dialog.setDefaultButton(static_cast<QMessageBox::StandardButton>(button_1));
    int answer = dialog.exec();

    if(answer == button_2){
        return 1;
    }
    else if(answer == button_3){
        return 2;
    }

    return 0;
}

///
/// \brief askUser is a simple wrapper for a QMessageBox
/// \param msg          message to display
/// \param button_1     text of first button
/// \param button_2     (optional) text of second button
/// \param button_3     (optional) text of third button
/// \return             0 if user presses button_1, 1 for button_2 and 2 for button_3
///
inline int askUser(const QString& msg, const QString& button_1, const QString& button_2 = "", const QString& button_3 = ""){
    QMessageBox dialog;
    dialog.setText(msg);
    dialog.addButton(button_1, QMessageBox::AcceptRole);
    if(button_2.size() != 0){
        dialog.addButton(button_2, QMessageBox::RejectRole);
    }
    if(button_3.size() != 0){
        dialog.addButton(button_3, QMessageBox::DestructiveRole);
    }
    return dialog.exec();
}

///
/// \brief dialogYesNo opens a simple yes/no dialog
/// \param msg      message to display
/// \return         true if user clicks yes, false otherwise
///
inline bool dialogYesNo(const QString& msg){
    return (0==askUser(msg,QMessageBox::Yes,QMessageBox::No));
}

///
/// \brief dialogOkCancel opens a simple ok/cancel dialog
/// \param msg      message to display
/// \return         true if user clicks Ok, false otherwise
///
inline bool dialogOkCancel(const QString& msg){
    return (0==askUser(msg,QMessageBox::Ok,QMessageBox::Cancel));
}

///
/// \brief info opens a simple dialog for user information with a OK button
/// \param msg
///
inline void info(const QString& msg){
    QMessageBox dialog;
    dialog.setWindowTitle("Info");
    dialog.setText(msg);
    dialog.addButton(QMessageBox::Ok);
    dialog.exec();
}

///
/// \brief cvMatToQPixmap
/// \param image
/// \return
///
inline QPixmap cvMatToQPixmap(const cv::Mat& image){
    QImage::Format format;
    if(image.type() == CV_8UC1){
        format = QImage::Format_Grayscale8;
    }
    else if(image.type() == CV_8UC3){
        format = QImage::Format_RGB888;
    }
    else if(image.type() == CV_8UC4){
        format = QImage::Format_RGBA8888;
    }
    else if(image.type() == CV_32FC3){
        format = QImage::Format_RGB888;
        cv::Mat colored_image;
        image.convertTo(colored_image,CV_8UC3, 512.0, 0);
        return QPixmap::fromImage(QImage(colored_image.data, colored_image.cols, colored_image.rows, colored_image.step, format));

    }
    return QPixmap::fromImage(QImage(image.data, image.cols, image.rows, image.step, format));
}

///
/// \brief wavelengthToRGB
/// \param wavelength
/// \return
///
inline QColor wavelengthToRGB(float wavelength){
    //following Dan Brutons algorithm http://www.physics.sfasu.edu/astro/color/spectra.html
    QColor color(0,0,0);
    if(wavelength < 380.0){
        return color;
    }
    else if(wavelength < 440.0){
        color = QColor(255.0*(440.0-wavelength)/(440.0-380.0),0,255);
    }
    else if(wavelength < 490.0){
        color = QColor(0,255.0*(wavelength-440.0)/(490.0-440.0),255);
    }
    else if(wavelength < 510.0){
        color = QColor(0,255,255.0*(510.0-wavelength)/(510.0-490.0));
    }
    else if(wavelength < 580.0){
        color = QColor(255.0*(wavelength-510.0)/(580.0-510.0),255,0);
    }
    else if(wavelength < 645.0){
        color = QColor(255,255.0*(645.0-wavelength)/(645.0-580.0),0);
    }
    else if(wavelength < 780.0){
        color = QColor(255,0,0);
    }
    return color;
}



}

#endif // HELPER_H
