#include "spectrum_chart.h"

Spectrum_Chart::Spectrum_Chart(QObject* parent){

    QPen axisPen(QColor(204,102,0));
    axisPen.setWidth(2);
    axis_x_.setLinePen(axisPen);
    axis_y_.setLinePen(axisPen);
    axis_x_.setGridLineVisible(true);
    axis_y_.setGridLineVisible(true);

    chart_.addAxis(&axis_x_, Qt::AlignBottom);
    chart_.addAxis(&axis_y_, Qt::AlignLeft);
    chart_.legend()->hide();
    this->setChart(&chart_);

    setChartAxes();
}

void Spectrum_Chart::addSeries(QScatterSeries* series, const QColor& draw_color){
    series_.push_back(series);
    series_.back()->setPointsVisible(true);
    series_.back()->setColor(draw_color);
    chart_.addSeries(series_.back());
    chart_.setAxisX(&axis_x_,series_.back());
    chart_.setAxisY(&axis_y_,series_.back());
}

void Spectrum_Chart::addProfile(QLineSeries* profile, const QColor& draw_color, const bool& is_area, const QBrush& brush){
    if(profile != nullptr){
        if(!is_area){
            profiles_.push_back(profile);
            profiles_.back()->setPointsVisible(true);
            profiles_.back()->setColor(draw_color);
            chart_.addSeries(profiles_.back());
            chart_.setAxisX(&axis_x_,profiles_.back());
            chart_.setAxisY(&axis_y_,profiles_.back());
        }
        else{
            QAreaSeries* area_profile = new QAreaSeries();
            area_profile->setBrush(brush);
            area_profile->setBorderColor(draw_color);
            area_profile->setUpperSeries(profile);
            area_profiles_.push_back(area_profile);
            chart_.addSeries(area_profiles_.back());
            chart_.setAxisX(&axis_x_,area_profiles_.back());
            chart_.setAxisY(&axis_y_,area_profiles_.back());
        }
    }
}

void Spectrum_Chart::setChartAxes(float x_min, float x_max, int x_tick, float y_min, float y_max, int y_tick){
    axis_x_.setMin(x_min);
    axis_x_.setMax(x_max);
    axis_x_.setTickCount(x_tick);
    axis_y_.setMin(y_min);
    axis_y_.setMax(y_max);
    axis_y_.setTickCount(y_tick);
}

void Spectrum_Chart::mousePressEvent(QMouseEvent* event){
    if(event->button() == Qt::LeftButton || event->button() == Qt::MidButton){
        //check if any data point is nearby
        QPoint position = event->pos();
        QPointF chart_pos = mouseToChart(position);

        for(unsigned int series_idx = 0; series_idx < series_.size(); series_idx++){
            QScatterSeries* series = series_[series_idx];
            for(int element_idx = 0; element_idx < series->count(); element_idx++){
                const QPointF& p = series->at(element_idx);
                if(std::fabs(chart_pos.x()-p.x()) < 10.0){
                    QPoint q = chartToMouse(p);
                    if(std::fabs(position.x()-q.x()) < 7 && std::fabs(position.y()-q.y()) < 7){
                        if(event->button() == Qt::LeftButton){
                            active_data_point_idx_ = pairInt(series_idx,element_idx);
                        }
                        else{
                            active_data_point_idx_ = pairInt(series_idx,ACTIVE_ALL);
                        }
                        break;
                    }
                }
            }
        }

        last_chart_position_ = chart_pos;
    }
}

void Spectrum_Chart::mouseReleaseEvent(QMouseEvent* event){
    active_data_point_idx_ = pairInt(ACTIVE_NONE,ACTIVE_NONE);
}

void Spectrum_Chart::mouseMoveEvent(QMouseEvent* event){
    if(active_data_point_idx_.first != ACTIVE_NONE && active_data_point_idx_.second != ACTIVE_ALL){
        QPointF chart_pos = mouseToChart(event->pos());
        QScatterSeries* series_to_change = series_[active_data_point_idx_.first];
        chart_pos.setX(series_to_change->at(active_data_point_idx_.second).x());
        series_to_change->removePoints(active_data_point_idx_.second,1);
        series_to_change->insert(active_data_point_idx_.second,chart_pos);
        emit chartChanged();
    }
    else if(active_data_point_idx_.second == ACTIVE_ALL){
        QPointF chart_pos = mouseToChart(event->pos());
        QScatterSeries* series_to_change = series_[active_data_point_idx_.first];
        float diff = chart_pos.y() - last_chart_position_.y();
        for(int i = 0; i < series_to_change->count(); i++){
            series_to_change->append(series_to_change->at(0).x(),series_to_change->at(0).y()+diff);
            series_to_change->remove(0);
        }
        last_chart_position_ = chart_pos;
        emit chartChanged();
    }
}
