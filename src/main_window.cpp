#include "main_window.h"
#include "ui_main_window.h"
#include "helper.h"

#include <QFileDialog>

using namespace std;

Main_Window::Main_Window(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::Main_Window),
    mixer_(&data_)
{
    ui_->setupUi(this);
    ui_->listWidget->setIconSize(QSize(128,128));

    QLinearGradient gradient(0,0,1,0);
    for(float i = 400.0; i < 761.0; i+=5.0){
        gradient.setColorAt((i-400.0)/360.0, Helper::wavelengthToRGB(i));
    }
    gradient.setCoordinateMode(QGradient::ObjectBoundingMode);

    ui_->spectrum_emission->addProfile(&data_.emission_profile, QColor(0,0,0), true, gradient);
    ui_->spectrum_emission->addSeries(&data_.emission_coefficients);

    ui_->spectrum_sensor->addSeries(data_.sensor_coefficients[0], QColor(255,0,0));
    ui_->spectrum_sensor->addSeries(data_.sensor_coefficients[1], QColor(0,255,0));
    ui_->spectrum_sensor->addSeries(data_.sensor_coefficients[2], QColor(0,0,255));

    ui_->spectrum_sensor->addProfile(data_.sensor_profile[0], QColor(255,0,0));
    ui_->spectrum_sensor->addProfile(data_.sensor_profile[1], QColor(0,255,0));
    ui_->spectrum_sensor->addProfile(data_.sensor_profile[2], QColor(0,0,255));

    connect(ui_->button_load_images, &QPushButton::clicked, this, &Main_Window::loadImages);
    connect(ui_->listWidget, &QListWidget::itemChanged, this, &Main_Window::itemClicked);

    connect(this, &Main_Window::listUpdated, &mixer_, &Color_Mixer::mix);
    connect(this, &Main_Window::itemStateChanged, &mixer_, &Color_Mixer::mix);
    connect(this, &Main_Window::spectrumUpdated, &mixer_, &Color_Mixer::mix);

    connect(ui_->spectrum_emission, &Spectrum_Chart::chartChanged, &mixer_, &Color_Mixer::mix);
    connect(ui_->spectrum_sensor, &Spectrum_Chart::chartChanged, &mixer_, &Color_Mixer::mix);

    connect(&mixer_, &Color_Mixer::mixDone, ui_->graphicsView_result, &Qt_Tools::Graphics_View::setCvImage);

    //load spectrum profiles and set the corresponding comboboxes
    connect(ui_->comboBox_emission_profile, qOverload<int>(&QComboBox::currentIndexChanged), this, &Main_Window::changeSpectrumProfile);
    connect(ui_->comboBox_sensor_profile, qOverload<int>(&QComboBox::currentIndexChanged), this, &Main_Window::changeSpectrumProfile);
    data_.loadInitialProfiles();
    for(const Profile& profile : data_.emission_profiles){
        ui_->comboBox_emission_profile->addItem(QString::fromStdString(profile.name));
    }
    for(const Profile& profile : data_.sensor_profiles){
        ui_->comboBox_sensor_profile->addItem(QString::fromStdString(profile.name));
    }
    connect(ui_->button_add_emission_profile, &QPushButton::clicked, this, &Main_Window::profileAction);
    connect(ui_->button_add_sensor_profile, &QPushButton::clicked, this, &Main_Window::profileAction);
    connect(ui_->button_delete_emission_profile, &QPushButton::clicked, this, &Main_Window::profileAction);
    connect(ui_->button_delete_sensor_profile, &QPushButton::clicked, this, &Main_Window::profileAction);
    connect(ui_->button_save_emission_profiles, &QPushButton::clicked, this, &Main_Window::profileAction);
    connect(ui_->button_save_sensor_profiles, &QPushButton::clicked, this, &Main_Window::profileAction);

    connect(ui_->button_reset_to_emission_spectrum, &QPushButton::clicked, this, &Main_Window::resetToProfile);
    connect(ui_->button_reset_to_sensor_spectrum, &QPushButton::clicked, this, &Main_Window::resetToProfile);

    connect(ui_->slider_emission_intensity, &QSlider::valueChanged, this, &Main_Window::sliderMoved);
    connect(ui_->slider_gain_red, &QSlider::valueChanged, this, &Main_Window::sliderMoved);
    connect(ui_->slider_gain_green, &QSlider::valueChanged, this, &Main_Window::sliderMoved);
    connect(ui_->slider_gain_blue, &QSlider::valueChanged, this, &Main_Window::sliderMoved);

}

Main_Window::~Main_Window(){
    delete ui_;
}

void Main_Window::loadImages(){
    //ask user to select images
    QStringList file_names = QFileDialog::getOpenFileNames(this,"Select the input images","/home/tmichels/Desktop/Chromatic Aberration/Results");
    if(file_names.size() == 0){
        return;
    }
    else{
        if(data_.input_images.size() != 0){
            if(Helper::dialogYesNo("Do you want to replace the previously loaded input images?")){
                //delete image list
                while(ui_->listWidget->count()>0){
                    ui_->listWidget->takeItem(0);
                }
                data_.input_images.clear();
                data_.emission_coefficients.clear();
                data_.sensor_coefficients[0]->clear();
                data_.sensor_coefficients[1]->clear();
                data_.sensor_coefficients[2]->clear();
            }
        }

        for(const QString& file_name : file_names){
            int delim_pos = file_name.lastIndexOf(".");
            bool success = false;
            int wavelength = file_name.mid(delim_pos-3,3).toInt(&success);
            if(!success){
                continue;
            }

            cv::Mat image = cv::imread(file_name.toStdString(),cv::IMREAD_UNCHANGED);
            cv::cvtColor(image,image,CV_BGR2RGB);

            data_.input_images.push_back(Image(QString::number(wavelength)+" nm", wavelength, image));
            data_.emission_coefficients << QPointF(float(wavelength),1.0);
            (*data_.sensor_coefficients[0]) << QPointF(float(wavelength),1.0);
            (*data_.sensor_coefficients[1]) << QPointF(float(wavelength),1.0);
            (*data_.sensor_coefficients[2]) << QPointF(float(wavelength),1.0);
        }
    }

    ui_->button_reset_to_emission_spectrum->clicked();
    ui_->button_reset_to_sensor_spectrum->clicked();

    updateList();
}

void Main_Window::updateList(){
    while(ui_->listWidget->count()>0){
        ui_->listWidget->takeItem(0);
    }
    for(Image& image : data_.input_images){
        ui_->listWidget->insertItem(ui_->listWidget->count(),&image);
    }

    emit listUpdated();
}

void Main_Window::itemClicked(){
    bool changed = false;
    for(Image& image : data_.input_images){
        if(image.check_state != image.checkState()){
            image.check_state = image.checkState();
            changed = true;
        }
    }
    if(changed){
        emit itemStateChanged();
    }
}

void Main_Window::changeSpectrumProfile(int idx){
    QObject* sender = this->sender();

    if(sender == ui_->comboBox_emission_profile){
        data_.emission_profile.clear();
        const Profile& profile = data_.emission_profiles[idx];
        for(const QPointF& p : profile.data[0]){
            data_.emission_profile.insert(data_.emission_profile.count(),p);
        }
    }
    else if(sender == ui_->comboBox_sensor_profile){
        data_.sensor_profile[0]->clear();
        data_.sensor_profile[1]->clear();
        data_.sensor_profile[2]->clear();

        const Profile& profile = data_.sensor_profiles[idx];
        for(int i = 0 ; i < 3; i++){
            for(const QPointF& p : profile.data[i]){
                data_.sensor_profile[i]->insert(data_.sensor_profile[i]->count(),p);
            }
        }
    }
}

void Main_Window::resetToProfile(){
    QObject* sender = this->sender();

    if(sender == ui_->button_reset_to_emission_spectrum){
        int profile_idx = ui_->comboBox_emission_profile->currentIndex();
        const vector<QPointF>& profile_data = data_.emission_profiles[profile_idx].data[0];
        for(int i = 0; i < data_.emission_coefficients.count(); i++){
            QPointF p = data_.emission_coefficients.at(0);
            for(unsigned j = 0; j < profile_data.size(); j++){
                const QPointF& q = profile_data[j];
                if(p.x() <= q.x()){
                    if(j > 0){
                        p.setY(q.y()+(p.x()-q.x())*(profile_data[j-1].y()-q.y())/(profile_data[j-1].x()-q.x()));
                    }
                    else{
                        p.setY(q.y());
                    }

                    break;
                }
            }
            data_.emission_coefficients.append(p);
            data_.emission_coefficients.remove(0);
        }
    }
    else if(sender == ui_->button_reset_to_sensor_spectrum){
        int profile_idx = ui_->comboBox_sensor_profile->currentIndex();
        for(int channel_idx = 0; channel_idx < 3; channel_idx++){
            const vector<QPointF>& profile_data = data_.sensor_profiles[profile_idx].data[channel_idx];
            for(int i = 0; i < data_.sensor_coefficients[channel_idx]->count(); i++){
                QPointF p = data_.sensor_coefficients[channel_idx]->at(0);
                for(unsigned j = 0; j < profile_data.size(); j++){
                    const QPointF& q = profile_data[j];
                    if(p.x() <= q.x()){
                        if(j > 0){
                            p.setY(q.y()+(p.x()-q.x())*(profile_data[j-1].y()-q.y())/(profile_data[j-1].x()-q.x()));
                        }
                        else{
                            p.setY(q.y());
                        }

                        break;
                    }
                }
                data_.sensor_coefficients[channel_idx]->append(p);
                data_.sensor_coefficients[channel_idx]->remove(0);
            }
        }
    }

    emit spectrumUpdated();
}

void Main_Window::profileAction(){

}

void Main_Window::sliderMoved(int value){
    QObject* sender = this->sender();
    if(sender == ui_->slider_emission_intensity){
        data_.emission_intensity = float(value)/100.0;
        ui_->display_emission_intensity->setText(QString::number(data_.emission_intensity));
    }
    else if(sender == ui_->slider_gain_red){
        data_.gain_r = float(value)/100.0;
        ui_->display_gain_red->setText(QString::number(data_.gain_r));
    }
    else if(sender == ui_->slider_gain_green){
        data_.gain_g = float(value)/100.0;
        ui_->display_gain_green->setText(QString::number(data_.gain_g));
    }
    else if(sender == ui_->slider_gain_blue){
        data_.gain_b = float(value)/100.0;
        ui_->display_gain_blue->setText(QString::number(data_.gain_b));
    }

    emit spectrumUpdated();
}

