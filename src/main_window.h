#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H
#pragma once

#include "data.h"
#include "color_mixer.h"

#include <QMainWindow>

namespace Ui {
class Main_Window;
}

class Main_Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Main_Window(QWidget *parent = nullptr);
    ~Main_Window();

private:
    Ui::Main_Window* ui_;
    Data data_;
    Color_Mixer mixer_;

private slots:

    void loadImages();
    void updateList();
    void itemClicked();
    void changeSpectrumProfile(int idx);
    void resetToProfile();
    void profileAction();
    void sliderMoved(int value);

signals:

    void listUpdated();
    void itemStateChanged();
    void spectrumUpdated();
    void addProfile();
};

#endif // MAIN_WINDOW_H
