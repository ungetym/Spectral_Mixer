#include "color_mixer.h"

using namespace std;
using namespace cv;

Color_Mixer::Color_Mixer(Data* data) : data_(data){

}

void Color_Mixer::mix(){
    if(data_->input_images.size() == 0){
        return;
    }
    cv::Mat* test_image = &data_->input_images[0].image_intensity;

    for(int i = 0; i < 3; i++){
        mixed_images_[i] = cv::Mat::zeros(test_image->rows,test_image->cols,test_image->type());
    }

    for(int i = 0; i < data_->emission_coefficients.count(); i++){
        const Image& image = data_->input_images[i];
        if(image.check_state == Qt::Checked){
            mixed_images_[0] += data_->emission_intensity*data_->gain_r*data_->emission_coefficients.at(i).y()*data_->sensor_coefficients[0]->at(i).y()*image.image_intensity;
            mixed_images_[1] += data_->emission_intensity*data_->gain_g*data_->emission_coefficients.at(i).y()*data_->sensor_coefficients[1]->at(i).y()*image.image_intensity;
            mixed_images_[2] += data_->emission_intensity*data_->gain_b*data_->emission_coefficients.at(i).y()*data_->sensor_coefficients[2]->at(i).y()*image.image_intensity;
        }
    }


    merge(mixed_images_,data_->mixed_image);

    emit mixDone(data_->mixed_image);
}
